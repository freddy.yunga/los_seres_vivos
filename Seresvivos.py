"""LOS SERES VIVOS  """
class Plantas:
    especies=" "
    Tipos=" "

    def __init__(self,especies,tipos):
        self.especies=especies
        self.tipos=tipos

    def concepto(self):
        print(" Una planta es un ser orgánico que vive y crece, "
              "pero sin mudar de lugar por impulso voluntario. "
              "Se trata de los vegetales como los árboles o las hortalizas, "
              "que constituyen el objeto de estudio de la botánica.")
    def mostrar_partes(self):
        print("Raíz: su función es fijar a la planta. "
              "Mediante ella las plantas obtienen nutrientes del suelo.")
        print("Hoja: es la estructura donde se realiza la fotosíntesis y la respiración.")
        print("Flor: es el órgano reproductor.")
        print("Tallo: es el que le da soporte a la planta")

    def mostrar_plantas_con_semilla(self):
        print(" las plantas con semilla son: ")

    def mostrar_planatas_sin_semilla(self):
        print(" las plantas sin semilla son: ")

    def __str__(self):
        return f"{self.tipos}:{self.mostrar_partes()}"

    #plan=Plantas()
class Animales_Vertebrados:
    mamiferos=""
    reptiles=""
    peces=""
    aves=""


# huesos, patas (atributos)
    def __init__(self,mamiferos,reptiles,peces,aves):
        self.mamiferos
        self.reptiles
        self.aves
        self.peces

    def respirar(self):
        print(" oxigeno")

    def alimentar(self):
        print(" según su dieta: carniboros y hervivos")

    def reproducir(self):
        print(" según cambios hormonales")

class Animales_Invertebrados:
    tipos=" "

    def __init__(self, tipos):
        self.tipos=tipos

    def concepto(self):
        print("Los animales invertebrados son aquellos que carecen de cuerda dorsal, "
              "columna vertebral y esqueleto interno. Es decir, no tienen huesos.")


if __name__ == '__main__':
    op=0
    salir=4
    #llamdos a metodos
    plan = Plantas("rosa","clavel")
    ani1=Animales_Vertebrados("vaca","caiman","pez","condor")
    ani2=Animales_Invertebrados("moluscos")
    while op!=salir:
        print("  <MENU> ")
        print(" 1: LAS PLANTAS >")
        print(" 2: ANIMALES VERTEBRADOS > ")
        print(" 3: ANIMALES INVERTEBRADOS > ")
        print(" 4: SALIR >>")
        op=int(input("Digite opcion >> "))
        if op==1:
           plan.concepto()
        elif op==2:
            ani1.alimentar()
        elif op==3:
            ani2.concepto()
        elif op==4:
            break